import org.apache.log4j.BasicConfigurator;

import com.codeminders.ardrone.ARDrone;
public class TESTDRONE {
	private static final long CONNECT_TIMEOUT = 3000;
	public static void main(String[] args) {
		//Pour la configuration log4j
		BasicConfigurator.configure();
		// TODO Auto-generated method stub
        ARDrone drone;
        try
        {
            // Create ARDrone object,
            // connect to drone and initialize it.
            drone = new ARDrone();
            drone.connect();
            drone.clearEmergencySignal();

            // Wait until drone is ready
            drone.waitForReady(CONNECT_TIMEOUT);

            // do TRIM operation
            drone.trim();

            // Take off
            System.err.println("Taking off");
            /*drone.takeOff();

            // Fly a little :)
            Thread.sleep(5000);*/
            /** @param left_right_tilt The left-right tilt (aka. "drone roll" or phi
            	     *            angle) argument is a percentage of the maximum inclination as
            	     *            configured here. A negative value makes the drone tilt to its
            	     *            left, thus flying leftward. A positive value makes the drone
            	     *            tilt to its right, thus flying rightward.
            	     * @param front_back_tilt The front-back tilt (aka. "drone pitch" or theta
            	     *            angle) argument is a percentage of the maximum inclination as
            	     *            configured here. A negative value makes the drone lower its
            	     *            nose, thus flying frontward. A positive value makes the drone
            	     *            raise its nose, thus flying backward. The drone translation
            	     *            speed in the horizontal plane depends on the environment and
            	     *            cannot be determined. With roll or pitch values set to 0, the
            	     *            drone will stay horizontal but continue sliding in the air
            	     *            because of its inertia. Only the air resistance will then make
            	     *            it stop.
            	     * @param vertical_speed The vertical speed (aka. "gaz") argument is a
            	     *            percentage of the maximum vertical speed as defined here. A
            	     *            positive value makes the drone rise in the air. A negative
            	     *            value makes it go down.
            	     * @param angular_speed The angular speed argument is a percentage of the
            	     *            maximum angular speed as defined here. A positive value makes
            	     *            the drone spin right; a negative value makes it spin left.*/
            //Avancer
            /*drone.move((float)0, (float)0.50, (float)0.0,(float) 0.00);
            Thread.sleep(1000);
            //Reculer
            drone.move((float)0, (float)-0.50, (float)0.0,(float) 0.00);
            Thread.sleep(1000);
            //Monter
            drone.move((float)0, (float)0.0, (float)0.5,(float) 0.00);
            Thread.sleep(1000);
            //Descendre
            drone.move((float)0, (float)0.0, (float)-0.5,(float) 0.00);
            Thread.sleep(1000);
            //Gauche
            drone.move((float)-0.5, (float)0.0, (float)0.0,(float) 0.00);
            Thread.sleep(1000);
            //Droite
            drone.move((float)0.5, (float)0.0, (float)0.0,(float) 0.00);
            Thread.sleep(1000);
            //Tourner Gauche
            drone.move((float)0, (float)0.0, (float)0.0,(float) -0.50);
            Thread.sleep(1000);
            //Tourner Droite
            drone.move((float)0, (float)0.0, (float)0.0,(float) 0.50);
            Thread.sleep(1000);
            */

            // Land
            System.err.println("Landing");
            /*drone.land();

            // Give it some time to land
            Thread.sleep(2000);*/
            
            // Disconnect from the done
            drone.disconnect();
            System.err.println("Deconnection");
        } catch(Throwable e)
        {
            e.printStackTrace();
        }
	}

}
